package com.app.tech.Snooz;

import android.graphics.Point;

import org.junit.Test;
import com.app.tech.Snooz.Utility.*;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ScaleUtilityUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void scale_oneToOne(){
        ScaleUtility su = new ScaleUtility();
        su.setM_referenceHeight(1536);
        su.setM_referenceWidth(2008);
        su.setM_imageHeight(59);
        su.setM_imageWidth(21);
        su.setM_screenHeight(1536);
        su.setM_screenWidth(2008);
        int[] res = su.getScaledWidthHeight();
        assert(res[0] == 21);
        assert(res[1] == 59);
    }

    @Test
    public void scale_oneToOneHalf(){
        ScaleUtility su = new ScaleUtility();
        su.setM_referenceHeight(1536);
        su.setM_referenceWidth(2008);
        su.setM_imageHeight(59);
        su.setM_imageWidth(21);
        su.setM_screenHeight(1536/2);
        su.setM_screenWidth(2008/2);
        int[] res = su.getScaledWidthHeight();
        System.out.printf("%d  %d%n", res[0], res[1]);
        assert(res[0] == 21/2);
        assert(res[1] == 59/2);
    }

    @Test
    public void scale_oneToTwo(){
        ScaleUtility su = new ScaleUtility();
        su.setM_referenceHeight(1536);
        su.setM_referenceWidth(2008);
        su.setM_imageHeight(59);
        su.setM_imageWidth(21);
        su.setM_screenHeight(1536*2);
        su.setM_screenWidth(2008*2);
        int[] res = su.getScaledWidthHeight();
        System.out.printf("%d  %d%n", res[0], res[1]);
        assert(res[0] == 21*2);
        assert(res[1] == 59*2);
    }

    @Test
    public void scale_widthOneHalf(){
        ScaleUtility su = new ScaleUtility();
        su.setM_referenceHeight(1536);
        su.setM_referenceWidth(2008);
        su.setM_imageHeight(59);
        su.setM_imageWidth(21);
        su.setM_screenHeight(1536);
        su.setM_screenWidth(2008/2);
        int[] res = su.getScaledWidthHeight();
        System.out.printf("%d  %d%n", res[0], res[1]);
        assert(res[0] == 21/2);
        assert(res[1] == 59);
    }

    @Test
    public void scale_heightOneHalf(){
        ScaleUtility su = new ScaleUtility();
        su.setM_referenceHeight(1536);
        su.setM_referenceWidth(2008);
        su.setM_imageHeight(59);
        su.setM_imageWidth(21);
        su.setM_screenHeight(1536/2);
        su.setM_screenWidth(2008);
        int[] res = su.getScaledWidthHeight();
        System.out.printf("%d  %d%n", res[0], res[1]);
        assert(res[0] == 21);
        assert(res[1] == 59/2);
    }
}
