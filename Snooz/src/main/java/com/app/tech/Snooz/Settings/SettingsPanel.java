package com.app.tech.Snooz.Settings;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.tech.Snooz.R;
import com.app.tech.Snooz.Utility.SimpleTimeStamp;
import com.app.tech.Snooz.Utility.StringConversionUtility;
import com.appyvet.materialrangebar.IRangeBarFormatter;
import com.appyvet.materialrangebar.RangeBar;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class SettingsPanel {
    public interface PanelFunctionCallback {
        void settingsPanelChangesAccepted(HashMap<String, SimpleTimeStamp> updatedSettings, DaysSelection chosenDays, boolean enabled);
        HashMap<String, SimpleTimeStamp> getSettingsForPanelCreation();
        DaysSelection getDaySelectionForPanelCreation();
        boolean AlarmEnabled();
    }

    private PanelFunctionCallback mPanCloseCb;
    private PopupWindow mPopupWindow;
    private ImageButton mSettingsButton;
    private RangeBar mRangebar;
    private HashMap<String,Button> mDaysButton;
    private Switch mSwitchAlarmEnable;

    private SimpleTimeStamp alarmTimeCache = null;
    private SimpleTimeStamp dawnTimeCache = null;
    private SimpleTimeStamp duskTimeCache = null;
    private DaysSelection daysSelectionCache = null;
    boolean alarmEnabledCache;

    public SettingsPanel(PanelFunctionCallback pc) {
        this.mPanCloseCb = pc;
    }

    private void cacheAlarmTimeStamp(int hour, int minute) {
        alarmTimeCache = new SimpleTimeStamp(hour, minute);
    }

    private void cacheDawnTimeStamp(int hour, int minute) {
        dawnTimeCache = new SimpleTimeStamp(hour, minute);
    }

    private void cacheDuskTimeStamp(int hour, int minute) {
        duskTimeCache = new SimpleTimeStamp(hour, minute);
    }

    private void setupAlarmFunctionality(final Context c, View customView) {

        // Get the alarm settings from the callback
        SimpleTimeStamp alarmSetting = mPanCloseCb.getSettingsForPanelCreation().get(AlarmClockSettings.ID_ALARM);

        // Access the alarm text view and set it up appropriately
        final TextView alarmTime = customView.findViewById(R.id.settings_panel_alarm_textview);
        alarmTime.setText(alarmSetting.getAsAmPmString());
        alarmTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the value from the settings
                SimpleTimeStamp alarmSetting = mPanCloseCb.getSettingsForPanelCreation().get(AlarmClockSettings.ID_ALARM);
                // If there is an already cached variable, use that instead
                if(alarmTimeCache != null){
                    alarmSetting = alarmTimeCache;
                }
                // Load the saved times from the settings
                int mHour = alarmSetting.mHour;
                int mMinute = alarmSetting.mMinute;

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(c,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // Cache the newly reported value
                                // It will get committed based on the user selecting the
                                // Yes / Ok button in the settings panel
                                cacheAlarmTimeStamp(hourOfDay, minute);

                                //StringConversionUtility.convert24HourTimeToAmPmString(hourOfDay, minute)
                                // Update the text view with the new data here
                                alarmTime.setText(new SimpleTimeStamp(hourOfDay,minute).getAsAmPmString());
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });
    }

    private void setupDaySelectionButtons(final Context c,  View customView){
        // Create and populate the hashmap
        mDaysButton = new HashMap<>();
        mDaysButton.put(DaysSelection.ID_DAY_MON,(Button)customView.findViewById(R.id.btn_day_mon));
        mDaysButton.put(DaysSelection.ID_DAY_TUE,(Button)customView.findViewById(R.id.btn_day_tue));
        mDaysButton.put(DaysSelection.ID_DAY_WED,(Button)customView.findViewById(R.id.btn_day_wed));
        mDaysButton.put(DaysSelection.ID_DAY_THURS,(Button)customView.findViewById(R.id.btn_day_thurs));
        mDaysButton.put(DaysSelection.ID_DAY_FRI,(Button)customView.findViewById(R.id.btn_day_fri));
        mDaysButton.put(DaysSelection.ID_DAY_SAT,(Button)customView.findViewById(R.id.btn_day_sat));
        mDaysButton.put(DaysSelection.ID_DAY_SUN,(Button)customView.findViewById(R.id.btn_day_sun));

        // Setup an onclick listener for each entry
        for(Map.Entry<String, Button> entry : mDaysButton.entrySet()) {
            final String key = entry.getKey();
            final Button button = entry.getValue();
            if(daysSelectionCache.getDay(key)){
                button.setBackground(button.getResources().getDrawable(R.drawable.day_selection_button_selected));
            }else {
                button.setBackground(button.getResources().getDrawable(R.drawable.day_selection_button));
            }

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    daysSelectionCache.toggleSelection(key);
                    System.out.println(key + " :: " + daysSelectionCache.getDay(key));
                    if(daysSelectionCache.getDay(key)){
                        view.setBackground(view.getResources().getDrawable(R.drawable.day_selection_button_selected));
                    }else {
                        view.setBackground(view.getResources().getDrawable(R.drawable.day_selection_button));
                    }
                }
            });
        }
    }

    public void setupSettingsPanel(final Context c) {
        // Clear the local caches
        alarmTimeCache = null;
        dawnTimeCache = null;
        duskTimeCache = null;
        // Create new days selection from the settings version
        daysSelectionCache = new DaysSelection(mPanCloseCb.getDaySelectionForPanelCreation());


        mSettingsButton = ((Activity) c).findViewById(R.id.button_settings);
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Hide the settings button
                mSettingsButton.setVisibility(View.INVISIBLE);

                // Initialize a new instance of LayoutInflater service
                LayoutInflater inflater = (LayoutInflater) c.getSystemService(LAYOUT_INFLATER_SERVICE);

                // Inflate the custom layout/view
                final View customView = inflater.inflate(R.layout.settings_layout, null);

                // Initialize a new instance of popup window
                mPopupWindow = new PopupWindow(
                        customView,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                // Get a reference for the custom view close button
                Button doneButton = customView.findViewById(R.id.button_settings_done);

                alarmEnabledCache = mPanCloseCb.AlarmEnabled();
                ((Switch)customView.findViewById(R.id.switch_alarm_enabled)).setChecked(alarmEnabledCache);
                ((Switch)customView.findViewById(R.id.switch_alarm_enabled)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        alarmEnabledCache = b;
                    }
                });

                // Set a click listener for the popup window close button
                doneButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Dismiss the popup window
                        mPopupWindow.dismiss();

                        HashMap<String, SimpleTimeStamp> updatedSettings = new HashMap<String, SimpleTimeStamp>();
                        updatedSettings.put(AlarmClockSettings.ID_DAWN, dawnTimeCache);
                        updatedSettings.put(AlarmClockSettings.ID_DUSK, duskTimeCache);
                        updatedSettings.put(AlarmClockSettings.ID_ALARM, alarmTimeCache);

                        mPanCloseCb.settingsPanelChangesAccepted(updatedSettings, daysSelectionCache, alarmEnabledCache);

                        // Make the settings button visible again
                        mSettingsButton.setVisibility(View.VISIBLE);
                    }
                });

                Button cancelButton = customView.findViewById(R.id.button_settings_cancel);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPopupWindow.dismiss();
                        // Make the settings button visible again
                        mSettingsButton.setVisibility(View.VISIBLE);
                    }
                });
                setupAlarmFunctionality(c, customView);

                // link the rangebar object
                mRangebar = customView.findViewById(R.id.sample_rangebar);

                // If the dusk / dawn time has been set, then load it up. Otherwise let it default
                // to the far right value
                SimpleTimeStamp duskSetting = mPanCloseCb.getSettingsForPanelCreation().get(AlarmClockSettings.ID_DUSK);
                SimpleTimeStamp dawnSetting = mPanCloseCb.getSettingsForPanelCreation().get(AlarmClockSettings.ID_DAWN);
                if (duskSetting.mHour * 60 + duskSetting.mMinute > 0) {
                    mRangebar.setRangePinsByValue(Math.max(240,dawnSetting.mHour * 60 + dawnSetting.mMinute),
                            Math.min(22*60+30,duskSetting.mHour * 60 + duskSetting.mMinute));
                    // Update the text views with the values from the settings here
                    TextView et = customView.findViewById(R.id.settings_panel_sunset_textview);
                    et.setText(c.getString(R.string.str_Sunset) + " " + duskSetting.getAsAmPmString());

                    et = customView.findViewById(R.id.settings_panel_sunrise_textview);
                    et.setText(c.getString(R.string.str_Sunrise) + " " + dawnSetting.getAsAmPmString());

                } else {
                    mRangebar.setRangePinsByValue(0,
                            24 * 60 - 1);
                }

                mRangebar.setFormatter(new IRangeBarFormatter() {
                    @Override
                    public String format(String s) {
                        int val = Integer.parseInt(s);
                        int hours = val / 60;
                        int minutes = val % 60;
                        // Transform the String s here then return s
                        return StringConversionUtility.convert24HourTimeToAmPmString(hours, minutes);
                    }
                });

                mRangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                    @Override
                    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                                      int rightPinIndex, String leftPinValue, String rightPinValue) {
                        // Update the Text View
                        TextView et = customView.findViewById(R.id.settings_panel_sunrise_textview);
                        int val = Integer.parseInt(leftPinValue);
                        int hours = val / 60;
                        int minutes = val % 60;
                        et.setText("Sunrise " + StringConversionUtility.convert24HourTimeToAmPmString(hours, minutes));

                        // Cache this selected value
                        cacheDawnTimeStamp(hours, minutes);

                        // Update the Text View
                        et = customView.findViewById(R.id.settings_panel_sunset_textview);
                        val = Integer.parseInt(rightPinValue);
                        hours = val / 60;
                        minutes = val % 60;
                        et.setText("Sunset " + StringConversionUtility.convert24HourTimeToAmPmString(hours, minutes));

                        // Cache this selected value
                        cacheDuskTimeStamp(hours, minutes);
                    }

                });

                setupDaySelectionButtons(c, customView);

                // Finally, show the popup window at the center location of root relative layout
                mPopupWindow.showAtLocation(mSettingsButton, Gravity.BOTTOM | Gravity.RIGHT, 16, 16);
            }
        });
    }
}
