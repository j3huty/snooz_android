package com.app.tech.Snooz.Clock;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.app.tech.Snooz.R;

import java.util.HashMap;
import java.util.Map;

public class DigitsUiManager {
    private Context mC;
    private Map<Integer, ImageView> mClockMap;
    private Map<Integer, Drawable> mDrawableMappingDay;
    private Map<Integer, Drawable> mDrawableMappingNight;
    private static final int HOURS_TENS = 0;
    private static final int HOURS_SINGLES = 1;
    private static final int MINUTES_TENS = 2;
    private static final int MINUTES_SINGLES = 3;
    private static final int COLON = 10;
    private boolean mNightTime = false;

    DigitsUiManager(Context c){
        mC = c;
        mClockMap = new HashMap<>();
        mClockMap.put(HOURS_TENS,(ImageView)((Activity) mC).findViewById(R.id.button_hour_tens));
        mClockMap.put(HOURS_SINGLES, (ImageView)((Activity) mC).findViewById(R.id.button_hour_singles));
        mClockMap.put(MINUTES_TENS, (ImageView)((Activity) mC).findViewById(R.id.button_minutes_tens));
        mClockMap.put(MINUTES_SINGLES, (ImageView)((Activity) mC).findViewById(R.id.button_minutes_singles));
        mClockMap.put(COLON, (ImageView)((Activity) mC).findViewById(R.id.button_hour_colon));

        mDrawableMappingDay = new HashMap<>();
        mDrawableMappingDay.put(0, mC.getResources().getDrawable(R.drawable.number_0));
        mDrawableMappingDay.put(1, mC.getResources().getDrawable(R.drawable.number_1));
        mDrawableMappingDay.put(2, mC.getResources().getDrawable(R.drawable.number_2));
        mDrawableMappingDay.put(3, mC.getResources().getDrawable(R.drawable.number_3));
        mDrawableMappingDay.put(4, mC.getResources().getDrawable(R.drawable.number_4));
        mDrawableMappingDay.put(5, mC.getResources().getDrawable(R.drawable.number_5));
        mDrawableMappingDay.put(6, mC.getResources().getDrawable(R.drawable.number_6));
        mDrawableMappingDay.put(7, mC.getResources().getDrawable(R.drawable.number_7));
        mDrawableMappingDay.put(8, mC.getResources().getDrawable(R.drawable.number_8));
        mDrawableMappingDay.put(9, mC.getResources().getDrawable(R.drawable.number_9));
        mDrawableMappingDay.put(COLON, mC.getResources().getDrawable(R.drawable.number_colon));

        mDrawableMappingNight = new HashMap<>();
        mDrawableMappingNight.put(0, mC.getResources().getDrawable(R.drawable.number_night_0));
        mDrawableMappingNight.put(1, mC.getResources().getDrawable(R.drawable.number_night_1));
        mDrawableMappingNight.put(2, mC.getResources().getDrawable(R.drawable.number_night_2));
        mDrawableMappingNight.put(3, mC.getResources().getDrawable(R.drawable.number_night_3));
        mDrawableMappingNight.put(4, mC.getResources().getDrawable(R.drawable.number_night_4));
        mDrawableMappingNight.put(5, mC.getResources().getDrawable(R.drawable.number_night_5));
        mDrawableMappingNight.put(6, mC.getResources().getDrawable(R.drawable.number_night_6));
        mDrawableMappingNight.put(7, mC.getResources().getDrawable(R.drawable.number_night_7));
        mDrawableMappingNight.put(8, mC.getResources().getDrawable(R.drawable.number_night_8));
        mDrawableMappingNight.put(9, mC.getResources().getDrawable(R.drawable.number_night_9));
        mDrawableMappingNight.put(COLON, mC.getResources().getDrawable(R.drawable.number_night_colon));
    }

    void updateUI(int hoursTens, int hoursSingles, int minutesTens, int minutesSingles){
        Map<Integer, Drawable> mp;
        if(this.mNightTime){
            mp = this.mDrawableMappingNight;
        } else {
            mp = this.mDrawableMappingDay;
        }
        Object mView = mClockMap.get(HOURS_TENS);
        if(hoursTens != 0) {
            ((ImageView) mView).setImageDrawable((mp.get(hoursTens)));
            ((ImageView) mView).setVisibility(ImageView.VISIBLE);
        } else {
            ((ImageView) mView).setVisibility(ImageView.INVISIBLE);
        }

        mView = mClockMap.get(HOURS_SINGLES);
        ((ImageView)mView).setImageDrawable((mp.get(hoursSingles)));

        mView = mClockMap.get(MINUTES_TENS);
        ((ImageView)mView).setImageDrawable((mp.get(minutesTens)));

        mView = mClockMap.get(MINUTES_SINGLES);
        ((ImageView)mView).setImageDrawable((mp.get(minutesSingles)));

        mView = mClockMap.get(COLON);
        ((ImageView) mView).setImageDrawable((mp.get(COLON)));
    }

    public void setmNightTime(boolean mNightTime) {
        this.mNightTime = mNightTime;
    }

}
