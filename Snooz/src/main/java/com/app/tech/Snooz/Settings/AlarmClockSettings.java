package com.app.tech.Snooz.Settings;

import android.content.Context;

import com.app.tech.Snooz.Utility.SimpleTimeStamp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AlarmClockSettings {
    private static final int VERSION = 1;
    private static final String ID_VERSION = "VERSION";

    public static final String ID_DAWN = "DAWN";
    public static final String ID_DUSK = "DUSK";
    public static final String ID_ALARM = "ALARM";
    private static final String ID_DAYS_SELECTION = "DAYS_SELECTION";
    private static final String ID_ALARM_ENABLED = "ALARM_STATUS";

    public DaysSelection mDaysSettings = new DaysSelection();

    public static final int DAWY = 0;
    public static final int DUSK = 1;
    public static final int DAY = 2;
    public static final int NIGHT = 3;
    public static final int NIGHT_LIGHT = 4;
    private SimpleTimeStamp mAlarmTime = new SimpleTimeStamp();
    private SimpleTimeStamp mDawnTime = new SimpleTimeStamp();
    private SimpleTimeStamp mDuskTime = new SimpleTimeStamp();
    public boolean mAlarmEnabled = false;
    private int mState = DAY;

    private static final int ALARM_IDLE = 0;
    private static final int ALARM_PLAYING = 1;
    private static final int ALARM_TRIGGER = 2;
    private static final int ALARM_CANCELLED = 3;
    private int mAlarmState = ALARM_IDLE;

    public AlarmClockSettings() {
        mDawnTime.mHour = 6;
        mDawnTime.mMinute = 30;
        mDuskTime.mHour = 18;
        mDuskTime.mMinute = 0;
        mAlarmTime.mHour = 6;
        mAlarmTime.mMinute = 30;
    }

    public void triggerAlarm() {
        if (mAlarmState != ALARM_CANCELLED)
            mAlarmState = ALARM_TRIGGER;
    }

    public void silenceTheAlarm() {
        if(mAlarmState != ALARM_TRIGGER)
            mAlarmState = ALARM_IDLE;
    }

    /**
     * Should the Alarm be playing right now? This is determined based on the
     * saved state of the settings (aka everything in the settings panel)
     *
     * @return true or false
     */
    public boolean shouldAlarmPlayNow() {
        // If the alarm is not enabled, it should never be playing
        if (!mAlarmEnabled)
            return false;

        // If the alarm has already been cancelled then it should not play now
        if (mAlarmState == ALARM_CANCELLED)
            return false;

        // If the alarm has been triggered, then it should only play if the
        // current day has been selected
        if (mAlarmState == ALARM_TRIGGER) {
            return this.alarmScheduledToday();
        }

        return false;
    }

    /**
     * Find out if there is an alarm scheduled to play today
     *
     * @return true or false
     */
    public boolean alarmScheduledToday() {
        Calendar calendar = Calendar.getInstance();
        // This format will return day of week as an integer number 1-7
        SimpleDateFormat mdformat = new SimpleDateFormat("E", Locale.ENGLISH);
        String strDate = mdformat.format(calendar.getTime());
        //int dayInWeek = Integer.parseInt(strDate);
        switch (strDate) {
            case "Mon":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_MON));
            case "Tue":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_TUE));
            case "Wed":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_WED));
            case "Thu":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_THURS));
            case "Fri":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_FRI));
            case "Sat":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_SAT));
            case "Sun":
                return (mDaysSettings.getDay(DaysSelection.ID_DAY_SUN));
            default:
                return false;
        }
    }

    public boolean isAlarmEnabled(){
        return mAlarmEnabled;
    }

    public void cancelAlarm() {
        mAlarmState = ALARM_CANCELLED;
    }

    public int getDayState() {
        return mState;
    }

    public void setDayState(int state) {
        mState = state;
    }

    public SimpleTimeStamp getmAlarmTime() {
        return mAlarmTime;
    }

    public void setmAlarmTime(SimpleTimeStamp mAlarmTime) {
        this.mAlarmTime = mAlarmTime;
    }

    public SimpleTimeStamp getmDawnTime() {
        return mDawnTime;
    }

    public void setmDawnTime(SimpleTimeStamp mDawnTime) {
        this.mDawnTime = mDawnTime;
    }

    public SimpleTimeStamp getmDuskTime() {
        return mDuskTime;
    }

    public String getDuskTimeAsString() {
        return getmDuskTime().getAsAmPmString();
    }

    public String getDawnTimeAsString() {
        return getmDawnTime().getAsAmPmString();
    }

    public void setmDuskTime(SimpleTimeStamp mDuskTime) {
        this.mDuskTime = mDuskTime;
    }

    public DaysSelection getDaysSettings() {
        return mDaysSettings;
    }

    public void backupSettings(Context c) {
        // Write all of the alarm settings here
        ApplicationProperties prop = ApplicationProperties.getInstance(c);
        prop.setProperties(ID_VERSION, Integer.toString(VERSION));
        prop.setProperties(ID_ALARM, this.getmAlarmTime().toString());
        prop.setProperties(ID_DAWN, this.getmDawnTime().toString());
        prop.setProperties(ID_DUSK, this.getmDuskTime().toString());
        prop.setProperties(ID_DAYS_SELECTION, this.getDaysSettings().toString());
        prop.setProperties(ID_ALARM_ENABLED, mAlarmEnabled ? "true" : "false");
    }

    public void retrieveSettings(Context c) {
        // Write all of the alarm settings here
        ApplicationProperties prop = ApplicationProperties.getInstance(c);
        this.mDuskTime.fromString(prop.getValue(ID_DUSK));
        this.mDawnTime.fromString(prop.getValue(ID_DAWN));
        this.mAlarmTime.fromString(prop.getValue(ID_ALARM));
        this.mDaysSettings.fromString(prop.getValue(ID_DAYS_SELECTION));
        this.mAlarmEnabled = Boolean.parseBoolean(prop.getValue(ID_ALARM_ENABLED));
    }

}
