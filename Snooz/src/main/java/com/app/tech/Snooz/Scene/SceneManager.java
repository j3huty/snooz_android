package com.app.tech.Snooz.Scene;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.app.tech.Snooz.Settings.AlarmClockSettings;
import com.app.tech.Snooz.Utility.SimpleTimeStamp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SceneManager {
    private BackgroundSceneUiManager mBackground;
    private AlarmClockSettings mSettings;
    private ForeGroundSceneUiManager mForeground;

    public SceneManager(AppCompatActivity c, final AlarmClockSettings settings) {
        mForeground = new ForeGroundSceneUiManager(c, settings);

        BackgroundSceneUiManager.BackgroundUiFunctionCallback mBkGrndCb = new BackgroundSceneUiManager.BackgroundUiFunctionCallback() {
            @Override
            public boolean cogDisplayed() {
                return true;
            }

            @Override
            public boolean alarmIconDisplayed() {
                return settings.alarmScheduledToday() && settings.isAlarmEnabled();
            }

            @Override
            public int getDayMode() {
                int mode = AlarmClockSettings.DAY;
                switch (settings.getDayState()) {
                    case AlarmClockSettings.DAWY:
                    case AlarmClockSettings.DUSK:
                    case AlarmClockSettings.DAY:
                        mode = AlarmClockSettings.DAY;
                        break;
                    case AlarmClockSettings.NIGHT:
                        mode = AlarmClockSettings.NIGHT;
                        break;
                    case AlarmClockSettings.NIGHT_LIGHT:
                        mode = AlarmClockSettings.NIGHT_LIGHT;
                        break;
                    default:
                        break;
                }
                return mode;
            }
        };
        mBackground = new BackgroundSceneUiManager(c, mBkGrndCb);
        mBackground.updateScene(false);
        mSettings = settings;
        // TODO: May want to remove this ...
        System.gc();
    }

    private void tickAppLogic(AlarmClockSettings settings){
        // Get the current time
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        String strDate = mdformat.format(calendar.getTime());
        String[] tk = strDate.split(":");
        int hours = Integer.parseInt(tk[0]);
        int minutes = Integer.parseInt(tk[1]);
        int seconds = Integer.parseInt(tk[2]);
        SimpleTimeStamp nowTime = new SimpleTimeStamp(hours, minutes, seconds);
        SimpleTimeStamp dawnTime = settings.getmDawnTime();
        SimpleTimeStamp duskTime = settings.getmDuskTime();
        SimpleTimeStamp alarmTime = settings.getmAlarmTime();
        if(nowTime.isBefore(dawnTime) || nowTime.isAfter(duskTime)){
            if(settings.getDayState() != AlarmClockSettings.NIGHT && settings.getDayState() != AlarmClockSettings.NIGHT_LIGHT) {
                settings.setDayState(AlarmClockSettings.NIGHT);
            }
        }
        if(nowTime.isAfter(dawnTime) && nowTime.isBefore(duskTime)){
            settings.setDayState(AlarmClockSettings.DAY);
        }
        nowTime.mSecond = 0;
        if(nowTime.isEqual(alarmTime)){
            settings.triggerAlarm();
        }else{
            settings.silenceTheAlarm();
        }
    }



    public void updateScene(Context c, AlarmClockSettings settings, boolean settingsChanged) {
        tickAppLogic(settings);
        mForeground.updateForeground();
        mBackground.updateScene(settingsChanged);
    }

}
