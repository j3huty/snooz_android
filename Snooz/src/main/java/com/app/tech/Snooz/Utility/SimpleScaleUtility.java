package com.app.tech.Snooz.Utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.View;

public class SimpleScaleUtility {

    public static void scaleFromDrawableIntrinsicSize(Context c,int viewId, int drawableId, float rawScaleFactor){

        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        View v = ((Activity)c).findViewById(viewId);

        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;
        Bitmap mBitmap = BitmapFactory.decodeResource(c.getResources(),drawableId, dimensions);
        int height = dimensions.outHeight;
        int width =  dimensions.outWidth;

        float rawNewHeight = height * rawScaleFactor;
        float rawNewWidth = width * rawScaleFactor;
        float fitHeight = rawNewHeight * metrics.widthPixels / 4267.0f;
        float fitWidth = rawNewWidth * metrics.widthPixels / 4267.0f;
        v.getLayoutParams().height = (int)fitHeight;
        v.getLayoutParams().width = (int)fitWidth;
        //System.out.println(drawableId + " : RawSize [" + rawNewWidth + "," + rawNewHeight + "] newSize [" + fitWidth + "," + fitHeight + "]");
    }
}
