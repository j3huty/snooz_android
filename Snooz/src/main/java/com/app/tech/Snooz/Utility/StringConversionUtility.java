package com.app.tech.Snooz.Utility;

import static java.lang.String.*;

public class StringConversionUtility {

    public static String convert24HourTimeToAmPmString(int hour, int minute) {
        String entry = format("%d:%02d", hour, minute);
        if (hour >= 12) {
            if( hour > 12){
                entry = format("%d:%02d", hour - 12, minute);
            }
            entry = entry + " PM";
        } else {
            entry = entry + " AM";
        }
        return entry;
    }
}
