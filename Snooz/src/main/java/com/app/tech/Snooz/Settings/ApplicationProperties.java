package com.app.tech.Snooz.Settings;

import com.app.tech.Snooz.Utility.Report;

import android.content.Context;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

/**
 * Created by j3 on 5/21/15.
 * This class is used to get properties and set properties. It is designed to perform a single file
 * IO on each utilization. Consider caching some of the preferences for faster access
 */
public class ApplicationProperties {
    private static final String PROPERTIES_KEY_TIMESTAMP = "time_stamp";
    private static final String PROPERTIES_FILE_NAME = "app.properties";
    private static volatile ApplicationProperties mRef = null;
    private static Properties mProperties = null;
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;

    private static Date mTimeStampofSetProperties;

    private ApplicationProperties(Context context) {

        if (context == null) throw new NullPointerException("Context must not be null");

        mContext = context;

        mProperties = new Properties();


        if (!propertiesFileExists()) {
            createFileInInternalStorage();
        }
    }

    /**
     * Get the instance of the ApplicationProperties. Use to set and get properties data (which is written to a private data file)
     *
     * @param context
     * @return the ApplicationProperties instance
     */
    public static ApplicationProperties getInstance(Context context) {
        if (mRef == null) {
            synchronized (ApplicationProperties.class) {
                if (mRef == null) {
                    mRef = new ApplicationProperties(context);
                }
            }
        }
        return mRef;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Search in private internal storage for the property file
     *
     * @return true if the property file exists
     */
    private boolean propertiesFileExists() {
        String[] files = mContext.fileList();
        for (String f : files) {
            if (f.equals(PROPERTIES_FILE_NAME)) {
                Report.d(TAG, "propertiesFile does exist");
                return true;
            }
        }
        Report.d(TAG, "propertiesFile does not exist");
        return false;
    }

    /**
     * Call this to ensure that the file is in internal storage
     */
    private void createFileInInternalStorage() {
        try {
            Report.d(TAG, "creating propertiesFile");
            mContext.openFileOutput(PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Store a name - value pair in the application properties
     *
     * @param name  the name of the value to be stored (key)
     * @param value the value to be stored (value)
     */
    public void setProperties(String name, String value) {
        if (mProperties != null) {

            mProperties.setProperty(name, value);
            try {
                FileOutputStream outputStream = mContext.openFileOutput(
                        PROPERTIES_FILE_NAME,
                        Context.MODE_PRIVATE);
                mProperties.store(outputStream, "Storing property");
                outputStream.close();


            } catch (IOException e) {
                e.printStackTrace();
            }
            setPropertiesTimeStamp();
        }
    }

    public void setPropertiesTimeStamp(){
        mTimeStampofSetProperties = new Date();
        if (mProperties != null) {

            mProperties.setProperty(ApplicationProperties.PROPERTIES_KEY_TIMESTAMP, mTimeStampofSetProperties.toString());
            try {
                FileOutputStream outputStream = mContext.openFileOutput(
                        PROPERTIES_FILE_NAME,
                        Context.MODE_PRIVATE);
                mProperties.store(outputStream, "Storing property");
                outputStream.close();


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param name         the name of the property to find.
     * @return the named property value.
     */
    public String getValue(String name) {
        Properties p = getProperties();
        return p.getProperty(name);
    }

    /**
     * Get the Properties object with the correct file. This will load up the properties on each occasion
     *
     * @return the properties object
     */
    private Properties getProperties() {
        //Must read the file properties every time otherwise the value may not be persistent
        try {

            InputStream inputStream = mContext.openFileInput(PROPERTIES_FILE_NAME);

            mProperties.load(inputStream);

            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return mProperties;
    }

    public void debugDirectories() {
        Report.d(TAG, mContext.getFilesDir().getPath());
    }

    public void debugInsert() {
        Properties p = getProperties();
        Report.d(TAG, "debugInsert");
        setProperties("Map_light", "mapbox.light");
        setProperties("Map_dark", "mapbox.dark");
    }

    public void debugRead() {
        Properties p = getProperties();
        Report.d(TAG, "debugRead");
        for (String key : p.stringPropertyNames()) {
            String v = p.getProperty(key);
            Report.d(TAG, "The value in " + key + " is " + v);
        }
    }
    public Properties debugReadProps() {
        Properties p = getProperties();
        return p;
    }
}

