package com.app.tech.Snooz.Utility;

import android.util.Log;

/**
 * Created by j3 on 5/21/15.
 * Wrapper for the Log.x methods. Uses a flag to enable or disable the Logging of the messages
 */
public class Report {
    private static volatile boolean DEBUG = false;

    public static void i(String TAG, String message) {
        if (!DEBUG) return;
        Log.i(TAG, message);
    }

    public static void d(String TAG, String message) {
        if (!DEBUG) return;
        Log.d(TAG, message);
    }

    public static void e(String TAG, String message) {
        if (!DEBUG) return;
        Log.e(TAG, message);
    }

    public static void v(String TAG, String message) {
        if (!DEBUG) return;
        Log.v(TAG, message);
    }

    public static void w(String TAG, String message) {
        if (!DEBUG) return;
        Log.w(TAG, message);
    }

}
