package com.app.tech.Snooz.Clock;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.app.tech.Snooz.R;
import com.app.tech.Snooz.Settings.AlarmClockSettings;
import com.app.tech.Snooz.Utility.DisplayMode;
import com.app.tech.Snooz.Utility.SimpleScaleUtility;

public class DismissAlarm implements DisplayMode {
    private ImageView mLargeBalloon1;
    private ImageView mSmallBaloon2;
    private boolean alreadyActive = false;
    private float mScreenWidth;
    private boolean mDayMode = true;
    private Context mC;

    public DismissAlarm(Context c, final AlarmClockSettings settings) {
        this.mC = c;
        mLargeBalloon1 = ((Activity) c).findViewById(R.id.hotair_baloon_1);
        mSmallBaloon2 =  ((Activity) c).findViewById(R.id.hotair_baloon_2);
        // Setup the onclick here. This will stop the alarm and dismiss the button
        mLargeBalloon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.cancelAlarm();
                mLargeBalloon1.setVisibility(ImageView.INVISIBLE);
                mLargeBalloon1.clearAnimation();
                mSmallBaloon2.setVisibility(ImageView.INVISIBLE);
                mSmallBaloon2.clearAnimation();
                alreadyActive = false;
            }
        });

        mSmallBaloon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.cancelAlarm();
                mLargeBalloon1.setVisibility(ImageView.INVISIBLE);
                mLargeBalloon1.clearAnimation();
                mSmallBaloon2.setVisibility(ImageView.INVISIBLE);
                mSmallBaloon2.clearAnimation();
                alreadyActive = false;
            }
        });
        mScreenWidth = c.getResources().getDisplayMetrics().widthPixels;

        mLargeBalloon1.setVisibility(ImageView.INVISIBLE);
        mLargeBalloon1.clearAnimation();
        mSmallBaloon2.setVisibility(ImageView.INVISIBLE);
        mSmallBaloon2.clearAnimation();

        // Scale the large hot air balloon
        SimpleScaleUtility.scaleFromDrawableIntrinsicSize(c,R.id.hotair_baloon_1,R.drawable.hot_air_balloon_1, 1.6f);

    }

    public void showAndAnimate() {
        if(alreadyActive)
            return;

        if(mDayMode){
            mLargeBalloon1.setImageDrawable(mC.getDrawable(R.drawable.hot_air_balloon_1));
            mSmallBaloon2.setImageDrawable(mC.getDrawable(R.drawable.hot_air_balloon_4));
        } else {
            mLargeBalloon1.setImageDrawable(mC.getDrawable(R.drawable.night_hot_air_balloon_1));
            mSmallBaloon2.setImageDrawable(mC.getDrawable(R.drawable.night_hot_air_balloon_4));
        }

        mLargeBalloon1.setVisibility(ImageView.VISIBLE);
        mLargeBalloon1.setX(-0.2f);
        mSmallBaloon2.setVisibility(ImageView.VISIBLE);
        mSmallBaloon2.setX(-0.2f);

        ObjectAnimator animation_1 = ObjectAnimator.ofFloat(mLargeBalloon1, "translationX", mScreenWidth + 250);
        animation_1.setDuration(60000);
        animation_1.setRepeatCount(100);
        animation_1.setRepeatMode(ObjectAnimator.RESTART);
        animation_1.start();

        ObjectAnimator animation_2 = ObjectAnimator.ofFloat(mSmallBaloon2, "translationX", mScreenWidth + 250);
        animation_2.setDuration(150000);
        animation_2.setRepeatCount(100);
        animation_2.setRepeatMode(ObjectAnimator.RESTART);
        animation_2.start();

        alreadyActive = true;
    }

    public void hideAndReset() {
        if(!alreadyActive)
            return;

        mLargeBalloon1.setVisibility(ImageView.INVISIBLE);
        mLargeBalloon1.clearAnimation();
        mSmallBaloon2.setVisibility(ImageView.INVISIBLE);
        mSmallBaloon2.clearAnimation();
        alreadyActive = false;
    }

    @Override
    public void setDayMode() {
        mDayMode = true;
    }

    @Override
    public void setNightMode() {
        mDayMode = false;
    }

    @Override
    public void setNightLightMode() {
        mDayMode = false;
    }
}
