package com.app.tech.Snooz.Scene;

import android.support.v7.app.AppCompatActivity;

import com.app.tech.Snooz.Clock.DigitsManager;
import com.app.tech.Snooz.Clock.DismissAlarm;
import com.app.tech.Snooz.Rooster.RoosterManager;
import com.app.tech.Snooz.Settings.AlarmClockSettings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Class to handle all of the foreground images. Currently this is the Rooster and
 * any other animations. Class will be self-contained and have callbacks to access necessary
 * application settings data
 */
public class ForeGroundSceneUiManager{
    private DigitsManager mClkManager;
    private RoosterManager mRooster;
    private AlarmClockSettings mSettings;
    private DismissAlarm mDismissAlarm;

    ForeGroundSceneUiManager(AppCompatActivity c, AlarmClockSettings settings){
        mClkManager = new DigitsManager(c);
        mRooster = new RoosterManager(c);
        mSettings = settings;
        mDismissAlarm = new DismissAlarm(c, settings);
        mDismissAlarm.hideAndReset();
    }

    public void updateDigits(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        String strDate = mdformat.format(calendar.getTime());
        String[] tk = strDate.split(":");
        int hours = Integer.parseInt(tk[0])%12;
        int minutes = Integer.parseInt(tk[1]);
        int seconds = Integer.parseInt(tk[2]);

        switch (mSettings.getDayState()){
            case AlarmClockSettings.NIGHT_LIGHT:
            case AlarmClockSettings.NIGHT:
                mClkManager.setNightMode();
                mDismissAlarm.setNightMode();
                break;
            default:
                mClkManager.setDayMode();
                mDismissAlarm.setDayMode();
                break;
        }
        mClkManager.setClockTo(hours, minutes);
    }

    public void updateRooster(){
        if(mSettings.shouldAlarmPlayNow()) {
            mDismissAlarm.showAndAnimate();
            switch (mSettings.getDayState()) {
                case AlarmClockSettings.DAWY:
                case AlarmClockSettings.DUSK:
                case AlarmClockSettings.DAY:
                    mRooster.dayCrow();
                    break;
                case AlarmClockSettings.NIGHT:
                case AlarmClockSettings.NIGHT_LIGHT:
                    mRooster.nightCrow();
                    break;
                default:
                    break;
            }

        }
        else {
            switch (mSettings.getDayState()) {
                case AlarmClockSettings.DAWY:
                case AlarmClockSettings.DUSK:
                case AlarmClockSettings.DAY:
                    mRooster.dayIdle();

                    break;
                case AlarmClockSettings.NIGHT:
                case AlarmClockSettings.NIGHT_LIGHT:
                    mRooster.nightIdle();
                    break;
                default:
                    break;
            }
            mDismissAlarm.hideAndReset();
        }
    }

    public void updateForeground(){
        updateDigits();
        updateRooster();
    }

}
