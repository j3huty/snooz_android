package com.app.tech.Snooz.Clock;

import android.support.v7.app.AppCompatActivity;

import com.app.tech.Snooz.Settings.AlarmClockSettings;
import com.app.tech.Snooz.Utility.DisplayMode;

public class DigitsManager implements DisplayMode{
    private DigitsUiManager mUi;
    private boolean mDayMode = true;

    public DigitsManager(AppCompatActivity c){
        mUi = new DigitsUiManager(c);
    }

    @Override
    public void setDayMode(){
        mDayMode = true;
    }

    @Override
    public void setNightMode(){
        mDayMode = false;
    }

    @Override
    public void setNightLightMode() {
        mDayMode = false;
    }

    /**
     * Pass in the Hour and the Minute to display.
     * The clock digits will update based on the new data and display the correct digits based on
     * the currently set day / night mode.
     * @param hours
     * @param minutes
     */
    public void setClockTo(int hours, int minutes){
        mUi.setmNightTime(!mDayMode);
        mUi.updateUI(hours/10, hours%10, minutes/10, minutes%10);
    }

}
