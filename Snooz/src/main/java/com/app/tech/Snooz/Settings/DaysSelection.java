package com.app.tech.Snooz.Settings;

import java.util.HashMap;
import java.util.Map;

public class DaysSelection {

    public static final String ID_DAY_SUN = "SUNDAY";
    public static final String ID_DAY_MON = "MONDAY";
    public static final String ID_DAY_TUE = "TUESDAY";
    public static final String ID_DAY_WED = "WEDNESDAY";
    public static final String ID_DAY_THURS = "THURSDAY";
    public static final String ID_DAY_FRI = "FRIDAY";
    public static final String ID_DAY_SAT = "SATURDAY";
    private HashMap<String, Boolean> mAlarmDays = new HashMap<>();

    /**
     * Copy data from one setting into this
     * @param ref Reference of day selection to copy
     */
    public DaysSelection(DaysSelection ref){
        mAlarmDays = new HashMap();
        for (Map.Entry<String, Boolean> entry : ref.mAlarmDays.entrySet())
        {
            mAlarmDays.put(entry.getKey(), entry.getValue());
        }
    }

    public DaysSelection() {
        mAlarmDays.put(ID_DAY_SUN, false);
        mAlarmDays.put(ID_DAY_MON, false);
        mAlarmDays.put(ID_DAY_TUE, false);
        mAlarmDays.put(ID_DAY_WED, false);
        mAlarmDays.put(ID_DAY_THURS, false);
        mAlarmDays.put(ID_DAY_FRI, false);
        mAlarmDays.put(ID_DAY_SAT, false);
    }

    public void toggleSelection(String day){
        if( mAlarmDays.get(day)){
            clearDay(day);
        }else {
            setDay(day);
        }
    }
    public void setDay(String day) {
        mAlarmDays.put(day, true);
    }

    public void clearDay(String day) {
        mAlarmDays.put(day, false);
    }

    public boolean getDay(String day){
        return mAlarmDays.get(day);
    }

    public String toString(){
        return  Boolean.toString(getDay(ID_DAY_MON)) + ',' +
                Boolean.toString(getDay(ID_DAY_TUE)) + ',' +
                Boolean.toString(getDay(ID_DAY_WED)) + ',' +
                Boolean.toString(getDay(ID_DAY_THURS)) + ',' +
                Boolean.toString(getDay(ID_DAY_FRI)) + ',' +
                Boolean.toString(getDay(ID_DAY_SAT)) + ',' +
                Boolean.toString(getDay(ID_DAY_SUN));
    }

    public void fromString(String s){
        if( s == null)
            return;
        String[] tokens = s.split(",");
        if(Boolean.parseBoolean(tokens[0])) setDay(ID_DAY_MON); else clearDay(ID_DAY_MON);
        if(Boolean.parseBoolean(tokens[1])) setDay(ID_DAY_TUE); else clearDay(ID_DAY_TUE);
        if(Boolean.parseBoolean(tokens[2])) setDay(ID_DAY_WED); else clearDay(ID_DAY_WED);
        if(Boolean.parseBoolean(tokens[3])) setDay(ID_DAY_THURS); else clearDay(ID_DAY_THURS);
        if(Boolean.parseBoolean(tokens[4])) setDay(ID_DAY_FRI); else clearDay(ID_DAY_FRI);
        if(Boolean.parseBoolean(tokens[5])) setDay(ID_DAY_SAT); else clearDay(ID_DAY_SAT);
        if(Boolean.parseBoolean(tokens[6])) setDay(ID_DAY_SUN); else clearDay(ID_DAY_SUN);
    }
}
