package com.app.tech.Snooz;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TimePicker;

import com.app.tech.Snooz.Scene.SceneManager;
import com.app.tech.Snooz.Settings.AlarmClockSettings;
import com.app.tech.Snooz.Settings.DaysSelection;
import com.app.tech.Snooz.Settings.SettingsPanel;
import com.app.tech.Snooz.Utility.SimpleTimeStamp;

import java.util.HashMap;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FarmSceneActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private final Handler mClockHandler = new Handler();
    private Runnable mClockRunner;
    private SceneManager mScneMaager;
    private AlarmClockSettings mAppSettings;
    private SettingsPanel mSettingPanel;
    private SettingsPanel.PanelFunctionCallback mPc;
    private boolean mSettingsChanged;

    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            mContentView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            // Set the content to appear under the system bars so that the
                            // content doesn't resize when the system bars hide and show.
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            // Hide the nav bar and status bar
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.show();
//            }
//            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            view.performClick();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.farm_scene);
        mSettingsChanged = false;
        mPc = new SettingsPanel.PanelFunctionCallback() {
            @Override
            public void settingsPanelChangesAccepted(HashMap<String, SimpleTimeStamp> updatedSettings, DaysSelection chosenDays, boolean enabled) {

                // If any of these have been modified, update the respective settings
                if(updatedSettings.get(AlarmClockSettings.ID_DAWN) != null)
                    mAppSettings.setmDawnTime(updatedSettings.get(AlarmClockSettings.ID_DAWN));
                if(updatedSettings.get(AlarmClockSettings.ID_DUSK)  != null)
                    mAppSettings.setmDuskTime(updatedSettings.get(AlarmClockSettings.ID_DUSK));
                if(updatedSettings.get(AlarmClockSettings.ID_ALARM)  != null)
                    mAppSettings.setmAlarmTime(updatedSettings.get(AlarmClockSettings.ID_ALARM));

                mAppSettings.mDaysSettings = new DaysSelection(chosenDays);
                mAppSettings.mAlarmEnabled = enabled;
                mAppSettings.backupSettings(getApplicationContext());
                mSettingsChanged = true;
                mClockHandler.removeCallbacks(mClockRunner);
                mClockHandler.postDelayed(mClockRunner, 100);
            }

            @Override
            public HashMap<String, SimpleTimeStamp> getSettingsForPanelCreation() {

                HashMap<String, SimpleTimeStamp> updatedSettings = new HashMap<>();
                updatedSettings.put(AlarmClockSettings.ID_ALARM,mAppSettings.getmAlarmTime());
                updatedSettings.put(AlarmClockSettings.ID_DAWN,mAppSettings.getmDawnTime());
                updatedSettings.put(AlarmClockSettings.ID_DUSK,mAppSettings.getmDuskTime());

                return updatedSettings;
            }

            @Override
            public DaysSelection getDaySelectionForPanelCreation(){
                return mAppSettings.mDaysSettings;
            }

            @Override
            public boolean AlarmEnabled() {
                return mAppSettings.isAlarmEnabled();
            }
        };
        mAppSettings = new AlarmClockSettings();
        mAppSettings.retrieveSettings(getApplicationContext());
        mSettingPanel = new SettingsPanel(mPc);
        mVisible = true;
        mControlsView = null;
        mContentView = findViewById(R.id.farmyard_background_scene_frame);

        // Initial Setup: Immersive Mode
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        mSettingPanel.setupSettingsPanel(this);

        FrameLayout clockLayout = new FrameLayout(this);
        clockLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        // Create the Scene Manager Object
        mScneMaager = new SceneManager(this, mAppSettings);

        // Repeatedly call the scene manager
        mClockHandler.postDelayed(mClockRunner = new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mScneMaager.updateScene(getApplicationContext(), mAppSettings, mSettingsChanged);
                        mSettingsChanged = false;
                    }
                });

                mClockHandler.postDelayed(mClockRunner, 1000);
            }
        }, 1000);

        mContentView.setVisibility(FrameLayout.VISIBLE);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if(this.mAppSettings.getDayState() == AlarmClockSettings.NIGHT){
            this.mAppSettings.setDayState(AlarmClockSettings.NIGHT_LIGHT);
        } else if(this.mAppSettings.getDayState() == AlarmClockSettings.NIGHT_LIGHT) {
            this.mAppSettings.setDayState(AlarmClockSettings.NIGHT);
        }
        // There has been a change so update the scene quickly
        mClockHandler.removeCallbacks(mClockRunner);
        mClockHandler.postDelayed(mClockRunner, 5);
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        //mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
