package com.app.tech.Snooz.Scene;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import com.app.tech.Snooz.R;
import com.app.tech.Snooz.Settings.AlarmClockSettings;
//import com.app.tech.Snooz.Utility.CropTransformation;
//import com.app.tech.Snooz.Utility.GlideApp;
import com.app.tech.Snooz.Utility.ScaleUtility;
import com.app.tech.Snooz.Utility.SimpleScaleUtility;
//import com.bumptech.glide.load.resource.bitmap.FitCenter;
//import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
//import com.bumptech.glide.request.target.Target;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class BackgroundSceneUiManager {
    private Context mC;
    private Bitmap dayImage;
    private Bitmap nightImage;
    private Bitmap nightLightImage;
    private int mCurrentMode;
    private int mLastMode;
    private int mScreenHeight;
    private int mScreenWidth;
    private int mLastDrawableId = -1;

    public interface BackgroundUiFunctionCallback {
        boolean cogDisplayed();

        boolean alarmIconDisplayed();

        int getDayMode();
    }

    private BackgroundUiFunctionCallback mFuncCallback;

    BackgroundSceneUiManager(Context c, BackgroundUiFunctionCallback cb) {
        this.mC = c;
        this.mFuncCallback = cb;

        WindowManager wm = (WindowManager) this.mC.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        DisplayMetrics dm = this.mC.getResources().getDisplayMetrics();
        display.getRealMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
    }

    private void setNightAlarmIcon() {
        ((ImageView) ((Activity) mC).findViewById(R.id.image_alarm_icon)).setImageDrawable(mC.getResources().getDrawable(R.drawable.icon_night_alarm));
        ((Activity) mC).findViewById(R.id.image_alarm_icon).setVisibility(ImageView.VISIBLE);
    }

    private void setDayCogIcon() {
        ((ImageView) ((Activity) mC).findViewById(R.id.button_settings)).setImageDrawable(mC.getResources().getDrawable(R.drawable.icon_day_cog));
    }

    private void setNightCogIcon() {
        ((ImageView) ((Activity) mC).findViewById(R.id.button_settings)).setImageDrawable(mC.getResources().getDrawable(R.drawable.icon_night_cog));
    }

    private void setDayAlarmIcon() {
        ((ImageView) ((Activity) mC).findViewById(R.id.image_alarm_icon)).setImageDrawable(mC.getResources().getDrawable(R.drawable.icon_day_alarm));
        ((Activity) mC).findViewById(R.id.image_alarm_icon).setVisibility(ImageView.VISIBLE);
    }

    private void clearAlarmIcon() {
        ((Activity) mC).findViewById(R.id.image_alarm_icon).setVisibility(ImageView.INVISIBLE);
    }

    private void setBackgroundDay() {
        if(dayImage == null){
            dayImage = scaleImage(((ImageView) ((Activity) mC).findViewById(R.id.sceneFarmYard)), R.drawable.summer_day_farm);
            // TODO: May want to remove this ...
            System.gc();
        }
        ((ImageView) ((Activity) mC).findViewById(R.id.sceneFarmYard)).setImageDrawable(new BitmapDrawable(this.mC.getResources(),dayImage));

    }

    private void setBackgroundNight() {
        if(nightImage == null){
            nightImage = scaleImage(((ImageView) ((Activity) mC).findViewById(R.id.sceneFarmYard)), R.drawable.background_night);
            // TODO: May want to remove this ...
            System.gc();
        }
        ((ImageView) ((Activity) mC).findViewById(R.id.sceneFarmYard)).setImageDrawable(new BitmapDrawable(this.mC.getResources(),nightImage));
    }

    private void setBackgroundNightLight() {
        if(nightLightImage == null) {
            nightLightImage = scaleImage(((ImageView) ((Activity) mC).findViewById(R.id.sceneFarmYard)), R.drawable.background_night_light);
            // TODO: May want to remove this ...
            System.gc();
        }
        ((ImageView) ((Activity) mC).findViewById(R.id.sceneFarmYard)).setImageDrawable(new BitmapDrawable(this.mC.getResources(),nightLightImage));
    }

    private Bitmap scaleImage(ImageView view, int id) throws NoSuchElementException {
        /*
        Load the image and get its width and height
        Get the width and height of the screen
        Scale the image to fit the smallest length to the screen
        Crop the scaled image and fit on the screen
         */

        // Extract the Screen Dimensions and the Image Dimensions
        int screenWidth = this.mScreenWidth;
        int screenHeight = this.mScreenHeight;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(mC.getResources(), id, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        // Get the relative scale for the vertical and horizontal
        float xScale = (float) screenWidth / (float) imageWidth;
        float yScale = (float) screenHeight / (float) imageHeight;

        // Select the smallest scale
        float scale = xScale <= yScale ? yScale : xScale;

        // Create a matrix for the scaling, use the chosen scale
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        BitmapFactory.Options imgOptions = new BitmapFactory.Options();
        imgOptions.inDither = true;
        imgOptions.inScaled = false;
        imgOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        Bitmap backgroundImage = BitmapFactory.decodeResource(mC.getResources(), id, imgOptions);

        Bitmap resbm = Bitmap.createScaledBitmap(backgroundImage, (int) (imageWidth * scale), (int) (imageHeight * scale), true);
        resbm = Bitmap.createBitmap(resbm, 0, (int) Math.abs((float) screenHeight - resbm.getHeight() + (250*scale)), screenWidth, screenHeight);
        // Apply the scaled bitmap

        view.setImageDrawable(new BitmapDrawable(resbm));
        view.setLayoutParams(new ConstraintLayout.LayoutParams(screenWidth, screenHeight));
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);

        BitmapFactory.Options myOptions = new BitmapFactory.Options();
        myOptions.inDither = true;
        myOptions.inScaled = false;
        myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        myOptions.inPurgeable = true;

        return resbm;
    }

    private void nightTimeScene(boolean nightLight) {
        if (nightLight) {
            setBackgroundNightLight();
        } else {
            setBackgroundNight();
        }
        if (this.mFuncCallback.alarmIconDisplayed()) {
            setNightAlarmIcon();
        } else {
            clearAlarmIcon();
        }
        setNightCogIcon();
    }

    private void dayTimeScene() {
        setBackgroundDay();
        if (this.mFuncCallback.alarmIconDisplayed()) {
            setDayAlarmIcon();
        } else {
            clearAlarmIcon();
        }
        setDayCogIcon();
    }

    public void updateScene(boolean forceUiUpdate) {
        if(!forceUiUpdate)
            // Scene is the same so don't update it
            if (mCurrentMode == this.mFuncCallback.getDayMode()) {
                return;
            }

        switch (this.mFuncCallback.getDayMode()) {
            case AlarmClockSettings.DAY:
                dayTimeScene();
                break;
            case AlarmClockSettings.NIGHT:
                nightTimeScene(false);
                break;
            case AlarmClockSettings.NIGHT_LIGHT:
                nightTimeScene(true);
                break;
        }
        mLastMode = mCurrentMode;
        mCurrentMode = this.mFuncCallback.getDayMode();
    }
}
