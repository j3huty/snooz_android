package com.app.tech.Snooz.Rooster;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import com.app.tech.Snooz.R;
import com.app.tech.Snooz.Utility.SimpleScaleUtility;

import java.io.IOException;

public class RoosterManager {
    private final static int ID_NIGHT_IDLE = 0;
    private final static int ID_DAY_IDLE = 1;
    private final static int ID_NIGHT_ALARM = 2;
    private final static int ID_DAY_ALARM = 3;
    private Context mC;
    private Handler animationAudioHandler;
    private ImageView mRoosterImageView;
    private int state = -1;
    private MediaPlayer mp;
    private AssetFileDescriptor af;

    private Runnable mAnimationRunnable = new Runnable() {
        @Override
        public void run() {

            switch(state){
                case ID_DAY_IDLE:
                    mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_day_idle));
                    if(mp != null)
                        mp.reset();
                    break;
                case ID_NIGHT_IDLE:
                    mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_night_idle));
                    if(mp != null)
                        mp.reset();
                    break;
                case ID_DAY_ALARM:
                    mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_day_alarm));
                    if(mp != null && !mp.isPlaying())
                        try {
                            mp.setDataSource(af.getFileDescriptor(),af.getStartOffset(),af.getLength());
                            mp.prepare();
                            mp.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    break;
                case ID_NIGHT_ALARM:
                    mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_night_alarm));
                    if(mp != null && !mp.isPlaying())
                        try {
                            mp.setDataSource(af.getFileDescriptor(),af.getStartOffset(),af.getLength());
                            mp.prepare();
                            mp.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    break;
                default:
                    break;
            }
            ((Animatable)mRoosterImageView.getBackground()).start();
        }
    };


    public RoosterManager(AppCompatActivity c){
        mC  = c;
        mRoosterImageView = c.findViewById(R.id.animatable_rooster);
        this.hide();
        ScaleRooster();
        mp = new MediaPlayer();
        af = mC.getResources().openRawResourceFd(R.raw.snd_rooster);
        animationAudioHandler = new Handler();
        mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                // Set the idle animation based on the alarm state (day or night)
                switch(state) {
                    case ID_DAY_ALARM:
                        mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_day_idle));
                        break;
                    case ID_NIGHT_ALARM:
                        mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_night_idle));
                        break;
                    default:
                        break;
                }
                // Start the animation
                ((Animatable) mRoosterImageView.getBackground()).start();
                // error(-38, 0) thrown on API 22 if stop is called without it being playing
                if(mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                mp.reset();

                // Remove any potential call backs and post another handler action
                animationAudioHandler.removeCallbacks(mAnimationRunnable);
                animationAudioHandler.postDelayed(mAnimationRunnable,1000);
            }
        });

        this.dayIdle();

    }

    public void dayIdle(){
        if(state == ID_DAY_IDLE)
            return;
        this.show();
        mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_day_idle));
        animationAudioHandler.removeCallbacks(mAnimationRunnable);
        animationAudioHandler.postDelayed(mAnimationRunnable,50);
        state = ID_DAY_IDLE;
    }

    public void dayCrow(){
        if(state == ID_DAY_ALARM)
            return;
        this.show();
        mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_day_alarm));
        animationAudioHandler.removeCallbacks(mAnimationRunnable);
        animationAudioHandler.postDelayed(mAnimationRunnable,50);
        state = ID_DAY_ALARM;
        mRoosterImageView.bringToFront();
    }

    public void nightIdle(){
        if(state == ID_NIGHT_IDLE)
            return;
        this.show();
        mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_night_idle));
        animationAudioHandler.removeCallbacks(mAnimationRunnable);
        animationAudioHandler.postDelayed(mAnimationRunnable,50);
        state = ID_NIGHT_IDLE;
    }

    public void nightCrow(){
        if(state == ID_NIGHT_ALARM)
            return;
        this.show();
        mRoosterImageView.setBackground(mC.getDrawable(R.drawable.animation_crow_night_alarm));
        animationAudioHandler.removeCallbacks(mAnimationRunnable);
        animationAudioHandler.postDelayed(mAnimationRunnable,50);
        state = ID_NIGHT_ALARM;
        mRoosterImageView.bringToFront();
    }

    private void hide(){
        mRoosterImageView.setVisibility(ImageView.INVISIBLE);
    }

    private void show(){
        mRoosterImageView.setVisibility(ImageView.VISIBLE);
    }

    private void ScaleRooster() {
        // Setup the Scale Utility. Pass in the Screen Size and the
        // Original Image Size (which this is all referenced from)

        DisplayMetrics metrics = mC.getResources().getDisplayMetrics();

        SimpleScaleUtility.scaleFromDrawableIntrinsicSize(this.mC,R.id.animatable_rooster,R.drawable.day_crow_frame_1_2s, 3.5f);


        WindowManager wm = (WindowManager) this.mC.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics dm = this.mC.getResources().getDisplayMetrics();
        display.getRealMetrics(dm);

        ImageView roost = ((Activity) mC).findViewById(R.id.animatable_rooster);
        double v = 1150.0 / 2134.0;
        float frameLeft = (float) ((v * dm.widthPixels) - roost.getLayoutParams().width * 0.59);
        roost.setX(frameLeft);


        float heightPixels = dm.heightPixels;

        // Set the locatino of the rooster in the scaled image
        // Get the scale for this screen size
        double _scle = dm.widthPixels / 2134.0;
        // Get the adjusted height
        double _hgt = 2134.0 * _scle;
        // Based on the location of the rooster in the original image, get the new location
        double _newLct = 3246.0/2 * _scle;
        // Determine how far this is from the top
        double _fromNewTop = _hgt - _newLct;
        // Find the adjusted y location and offset by a fixed number of pixels (relative to the
        // original image)
        double _yy = -_fromNewTop +  heightPixels + (250*_scle);

        roost.setY((float)(_yy - roost.getLayoutParams().height*0.99));

    }

}
