package com.app.tech.Snooz.Utility;

import android.graphics.Point;

public class ScaleUtility {
    private int m_imageWidth = 0;
    private int m_imageHeight = 0;
    private int m_referenceWidth = 0;
    private int m_referenceHeight = 0;
    private int m_screenWidth = 0;
    private int m_screenHeight = 0;
    private double m_heightScaleFactor = 1.0;
    private double m_scaleFactor = 1.0;

    public void setM_imageWidth(int m_imageWidth) {
        this.m_imageWidth = m_imageWidth;
    }

    public void setM_imageHeight(int m_imageHeight) {
        this.m_imageHeight = m_imageHeight;
    }

    public void setM_referenceWidth(int m_referenceWidth) {
        this.m_referenceWidth = m_referenceWidth;
    }

    public void setM_referenceHeight(int m_referenceHeight) {
        this.m_referenceHeight = m_referenceHeight;
    }

    private double scale(int screenVal, int refVal, int objectVal){
        return objectVal * screenVal / refVal;
    }

    public void setM_screenWidth(int m_screenWidth) {
        this.m_screenWidth = m_screenWidth;
    }

    public void setM_screenHeight(int m_ref_Height) {
        this.m_screenHeight = m_ref_Height;
    }

    public int[] getScaledWidthHeight(){
        int width = (int) (this.scale(this.m_screenWidth, this.m_referenceWidth, this.m_imageWidth) * this.m_scaleFactor);
        int height = (int) (this.scale(this.m_screenHeight, this.m_referenceHeight, this.m_imageHeight) *this.m_heightScaleFactor * this.m_scaleFactor);
        int [] res = {width, height};
        return res;
    }
    public Point getScaledPoint(){
        Point p = new Point();
        p.x = (int)this.scale(this.m_screenWidth, this.m_referenceWidth, this.m_imageWidth);
        p.y = (int)this.scale(this.m_screenHeight, this.m_referenceHeight, this.m_imageHeight);
        return p;
    }

    public void setM_heightScaleFactor(double m_heightScaleFactor) {
        this.m_heightScaleFactor = m_heightScaleFactor;
    }

    public void setM_scaleFactor(double m_scaleFactor) {
        this.m_scaleFactor = m_scaleFactor;
    }
}
