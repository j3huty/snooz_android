package com.app.tech.Snooz.Utility;

public class SimpleTimeStamp {

    public int mHour = 0;
    public int mMinute = 0;
    public int mSecond = 0;
    public boolean mSet = false;

    public SimpleTimeStamp(int hour, int minute){
        mHour = hour;
        mMinute = minute;
        mSecond = 0;
        mSet = true;
    }

    public SimpleTimeStamp(int hour, int minute, int second){
        mHour = hour;
        mMinute = minute;
        mSecond = second;
        mSet = true;
    }

    public SimpleTimeStamp(){

    }

    public String getAsAmPmString(){
        return StringConversionUtility.convert24HourTimeToAmPmString(mHour,mMinute);
    }

    public boolean isAfter(SimpleTimeStamp then){
        int thenSeconds = then.mHour*60*60 + then.mMinute*60 + then.mSecond;
        int thisSeconds = mHour*60*60 + mMinute*60 + mSecond;
        return thisSeconds > thenSeconds;
    }

    public boolean isBefore(SimpleTimeStamp then){
        int thenSeconds = then.mHour*60*60 + then.mMinute*60 + then.mSecond;
        int thisSeconds = mHour*60*60 + mMinute*60 + mSecond;
        return thisSeconds < thenSeconds;
    }

    public boolean isEqual(SimpleTimeStamp then){
        int thenSeconds = then.mHour*60*60 + then.mMinute*60 + then.mSecond;
        int thisSeconds = mHour*60*60 + mMinute*60 + mSecond;
        return thenSeconds == thisSeconds;
    }

    public String toString(){
        return Integer.toString(mHour) + ',' + Integer.toString(mMinute) + ',' + Integer.toString(mSecond) + ',' + mSet;
    }

    public void fromString(String s){
        if(s == null)
            return;
        String tok[] = s.split(",");
        this.mHour = Integer.parseInt(tok[0]);
        this.mMinute = Integer.parseInt(tok[1]);
        this.mSecond = Integer.parseInt(tok[2]);
        this.mSet = Boolean.parseBoolean(tok[3]);
    }
}
