package com.app.tech.Snooz.Utility;

public interface DisplayMode {
    void setDayMode();
    void setNightMode();
    void setNightLightMode();
}
